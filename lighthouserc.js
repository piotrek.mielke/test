module.exports = {
  ci: {
    collect: {
      numberOfRuns: 1,
      settings: {chromeFlags: '--no-sandbox --disable-dev-shm-usage'},
      url: ['https://google.com/'],
    },
   assert: {      
      assertions: {
        'categories:performance': ["error", {"minScore": 0.80}],
        'categories:pwa': 'off',
        'categories:accessibility': 'off',
      },
   },
    upload: {
       target: 'temporary-public-storage',
     },
  },
};
